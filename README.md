# CLion Example #

The purpose of this repository is to use it to demo some of the useful
features in CLion.

## Resources ##

* [CLion Tips and Tricks Confluence Document](https://iamrobotics.atlassian.net/l/c/g1uEamW0)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
