// IAM ROBOTICS LLC ("IAM Robotics") CONFIDENTIAL
// Unpublished Copyright (c) 2012-2020 IAM Robotics, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of IAM
// Robotics. The intellectual and technical concepts contained herein are
// proprietary to IAM Robotics and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from IAM
// Robotics.  Access to the source code contained herein is hereby forbidden to
// anyone except current IAM Robotics employees, managers or contractors who
// have executed Confidentiality and Non-disclosure agreements explicitly
// covering such access.
//
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF IAM ROBOTICS LLC IS STRICTLY PROHIBITED, AND IN VIOLATION OF
// APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS
// SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
//
// Created: November 2020
// Author: Nicholas Kirsch

#include "MyAbstractClass.h"
